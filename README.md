# Публичный портфолио

#### Привет 👋, коллега! ####

В этом репозитории находятся различные проекты: от *учебных* до **полноценных**, выполненных на заказ или для себя. Вы всегда можете посмотреть исходный код этих проектов и использовать в своей работе.

### Навигация по категориям:
* [Вёрстка сайтов](layouts-portfolio)
    * Проектов пока еще нет...
* [iOS-разработка](ios-portfolio)
    * [Flashlight - фонарик](ios-portfolio/flashlight)
* [VueJS-проекты](vuejs-portfolio)
    * [Игра "Убийца монстра"](vuejs-portfolio/monster-slayer-game)

#### Ссылки на профиль:

|  GitHub  |  GitLab  |
|:--------:|:--------:|
|[<img src="./assets/github_logo.png" width="30">](https://github.com/Johnsterr/)|[<img src="./assets/gitlab_logo.png" width="30">](https://gitlab.com/Sterr)|

**Ссылки для связи со мной:**
- <a href="mailto:info@eapashko.com/"><img src="./assets/icons/envelope-open.svg" width="16"> Почта</a>
- <a href="https://www.instagram.com/evgeniy.pashko/"><img src="./assets/icons/instagram.svg" width="16"> Instagram</a>
- <a href="https://www.facebook.com/evgpashko/"><img src="./assets/icons/facebook.svg" width="16"> Facebook</a>

<!--
**Johnsterr/Johnsterr** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

#### Portfolio Structure
```text
Portfolio Folder
├── ios-portfolio
│   └── No projects yet...
├── layouts-portfolio
│   └── No project yet...
└── vuejs-portfolio
    ├── crm-app
    ├── jedium-app
    └── notes-single-page-app
```

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...

│	
└── ios-portfolio
-->
