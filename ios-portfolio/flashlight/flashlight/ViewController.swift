//
//  ViewController.swift
//  flashlight
//
//  Created by Евгений Пашко on 27.04.2021.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    // MARK: - Properties
    var isLightOn = false
    var colorIndex = 0
    var buttonImage = "buttonOff" // Image name
    
    // Hide Status Bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
    // Disable Rotation
    override open var shouldAutorotate: Bool {
        return false
    }
    // Added Button from Storyboard
    @IBOutlet weak var lightTogglerButton: UIButton!
    
    // MARK: - Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    // Change Background Color after Touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        updateUI()
    }
    
    // Control the Device's Flashlight
    private func toggleLight() {
        let device = AVCaptureDevice.default(for: AVMediaType.video)
        if ((device?.hasTorch) != nil) {
            do {
                try device?.lockForConfiguration()
                device?.torchMode = isLightOn ? .on : .off
                device?.unlockForConfiguration()
            } catch {
                print("Flashlight could not be used")
            }
        } else {
            print("Flashlight is not available")
        }
    }
    
    // This function return new view's background color
    fileprivate func updateUI() {
        switch colorIndex {
        case 0: view.backgroundColor = .red
        case 1: view.backgroundColor = .yellow
        case 2: view.backgroundColor = .green
        default:
            break
        }
        colorIndex += 1
        if colorIndex == 3 {
            colorIndex = 0
        }
    }
    
    /// This function turn On/Off real devise's flashlight and change button image
    /// - Parameter sender: UIButton
    @IBAction private func lightToggler(_ sender: UIButton) {
        sender.setBackgroundImage(UIImage(named: buttonImage), for: UIControl.State.normal)
        isLightOn.toggle()
        buttonImage = isLightOn ? "buttonOn" : "buttonOff"
        toggleLight()
    }
}
