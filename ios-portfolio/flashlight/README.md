# Flashlight

Простое приложение для переключения цвета экрана и вспышки устройства.

### Функционал

- Изменение цвета экрана (Красный, Желтый, Зеленый)
- Включение и отключение вспышки устройства

*Обе функции являются независимыми друг от друга*

### Изображения

|  Launch Screen  |  Red Screen | Yellow Screen | Green Screen | Turn Off |
|:--------:|:--------:|:--------:|:--------:|:--------:|
|<img src="./images/launch_screen.png" width="120">|<img src="./images/red_screen.png" width="120">|<img src="./images/yellow_screen.png" width="120">|<img src="./images/green_screen.png" width="120">|<img src="./images/off_screen.png" width="120">|
